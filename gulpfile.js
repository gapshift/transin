// Gulp file

var gulp = require('gulp');
// var {series, task, watch, src, dest, pipe} = require('gulp');
var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
var gulpIf = require('gulp-if');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
const terser = require('gulp-terser');
const babel = require('gulp-babel');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');

gulp.task('watch', function () {
    gulp.watch('./src/sass/**/*.scss', gulp.series(['sass']));
    gulp.watch('./src/js/*.js', gulp.series(['javascript']));
});

// Sass task
gulp.task('sass', async function() {
    gulp.src('./src/sass/transin.scss')
        .pipe(
            plumber({
                errorHandler: function (err) {
                    notify.onError({
                        title: 'Gulp error in ' + err.plugin,
                        message: err.toString(),
                    })(err);
                },
            }),
        )
        .pipe(gulpIf('*.scss', sass()))
        .pipe(cleanCSS({ compatibility: 'ie11' }))
        .pipe(rename({ extname: '.min.css' }))
        .pipe(gulp.dest('./dist'));
});

// Javascript task
gulp.task('javascript', async function () {
    gulp.src(['./src/js/*.js'])
        .pipe(
            plumber({
                errorHandler: function (err) {
                    notify.onError({
                        title: 'Gulp error in ' + err.plugin,
                        message: err.toString(),
                    })(err);
                },
            }),
        )
        .pipe(
            babel({
                presets: [
                    [
                        '@babel/preset-env',
                        {
                            modules: false,
                        },
                    ],
                ],
            }),
        )
        .pipe(terser())
        .pipe(rename({ extname: '.min.js' }))
        .pipe(gulp.dest('./dist'));
});

// build task
gulp.task('build', gulp.series(['sass','javascript']), function(){
    gulp.src('./dist/*')
    pipe(notify('Built!'));
});