/**
 * Utility script to transition/animate elements into the viewport on scroll
 *
 * Author: Neil Coetzer
 * Author URL: https://www.neilcoetzer.com
 * Organisation: Gapshift (Pty) Ltd
 * Organisation URL: https://www.gapshift.com]
 *
 * version: 1.0
 * Distributed under the MIT License
 */

// Get all the elements that we need to observe
const animators = document.querySelectorAll('.animate');

// Set the config for observer
const config = {
	root: null,
	rootMargin: '0px 0px -15% 0px',
	threshold: [0],
};

observer = new IntersectionObserver(entries => {
	entries.forEach(entry => {
		// if the element is intersectiong the viewport
		if (entry.isIntersecting) {
			// check if a delay was set
			var delay = entry.target.dataset.animDelay;

			// if a delay was set, lets apply it
			if (delay) {
				setTimeout(function() {
					entry.target.classList.add('move');
				}, delay * 1000);
			} else {
				entry.target.classList.add('move');
			}
		}
	});
}, config);

if (animators) {
	// Loop through all specified items and observe them
	animators.forEach(elem => {
		observer.observe(elem);
	});
}
