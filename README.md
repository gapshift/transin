# README

##### v1.0

A utility script to enable animation of elements into the viewport on scroll.

## Getting started

To incorporate into your own sass flow, download or clone the repository locally and copy the files from the `src` directory to edit.

**OR**

1. Include the `transin.min.css` file from the `dist` directory into your application `<head>`

```HTML
...
<link rel="stylesheet" src="./directory-to-your-css/transin.min.js"></link>
</head>
```

2. Include the `transin.min.js` file before the `</body>` e.g.

```HTML
...
<script src="./directory-to-your-scripts/transin.min.js"></script>
</body>
```

## Usage

1. Add the `animate` class to any element you wish to animate into view.

2. Remember to add the `data-anim-direction` attribute to the element, specifying the direction of the animation with options: `left|right|up` e.g.

```HTML
 <div class="animate" data-anim-direction="up"></div>
```

3. (optional) To add a delay to the animation, add the `data-anim-delay` attribute, specifying the value in second e.g.

```HTML
 <div class="animate" data-anim-delay="0.2"></div>
```

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are greatly appreciated.

1. Clone the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## Contact

Neil Coetzer - more@gapshift.com
https://bitbucket.org/gapshift/transin
